package pe.goapps.gonews.utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import pe.goapps.gonews.app.GoNews;
import pe.goapps.gonews.model.Paper;

public class JsonUtils {
    public static String paperToJSonString(Paper paper) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("id", paper.getId());
            jsonObject.put("title", paper.getTitle());
            jsonObject.put("img", paper.getImg());
            jsonObject.put("country", paper.getCountry());
            jsonObject.put("lang", paper.getLang());
            jsonObject.put("url", paper.getUrl());
        } catch (Exception e) {

        }
        return jsonObject.toString();
    }


    public static Paper jsonToPaper(String jsonStr) {
        Paper paper = null;
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(jsonStr);
            paper = new Paper();
            paper.setId(jsonObject.getInt("id"));
            paper.setTitle(jsonObject.getString("title"));
            paper.setCountry(jsonObject.getString("country"));
            paper.setLang(jsonObject.getString("lang"));
            paper.setImg(jsonObject.getString("img"));
            paper.setUrl(jsonObject.getString("url"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return paper;
    }

    public static List<Paper> getPaperList(String json) {
        List<Paper> papers = new ArrayList<>();
        try {
            JSONArray arr = new JSONArray(json);
            for (int i = 0; i < arr.length(); i++) {
                Paper p = jsonToPaper(arr.get(i).toString());
                papers.add(p);
            }
        } catch (Exception e) {
            Log.e(GoNews.LOG_TAG, "Error fetching json filed");
        }
        return papers;
    }

    public static String getPaperListString() {
        String json = null;
        try {
            InputStream is = GoNews.getContext().getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (Exception e) {
            Log.e(GoNews.LOG_TAG, "Error fetching json filed");
        }

        return json;
    }
}



