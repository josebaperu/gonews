package pe.goapps.gonews.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.prof.rssparser.Article;
import com.prof.rssparser.Parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import pe.goapps.gonews.R;
import pe.goapps.gonews.model.Paper;
import pe.goapps.gonews.store.AppStorage;
import pe.goapps.gonews.utils.Utils;
import pe.goapps.gonews.views.FeedsActivity;
import pe.goapps.gonews.views.WebViewActivity;

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.PaperViewHolder> {
    private List<Article> articles = new ArrayList<Article>();
    private Context context;
    private TextToSpeech tts;
    private boolean isPLaying = false;
    private int id = -1;
    private Parser parser;
    private FeedsActivity activity;
    private String lang;
    private Paper paper;

    private final String COLOR1 = "#673AB7";
    private final String COLOR2 = "#708090";
    private final String COLOR3 = "#696969";

    public FeedsAdapter(Context context, Paper paper) {
        this.paper = paper;
        this.context = context;
        lang = AppStorage.getLang();
        activity = (FeedsActivity) context;
        getArticles();
    }

    public void stopTTS() {
        if (null != tts) {
            isPLaying = false;
            tts.stop();
            tts.shutdown();
        }
    }


    public class PaperViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        final TextView title;
        final TextView description;
        final TextView date;
        private final String LOG_TAG = "FeedsAdapter";

        PaperViewHolder(final View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cvFeed);
            title = (TextView) itemView.findViewById(R.id.titleFeed);
            date = (TextView) itemView.findViewById(R.id.dateFeed);
            description = (TextView) itemView.findViewById(R.id.descriptionFeed);
            cardView.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(View arg0) {
                    Intent intent = new Intent(context, WebViewActivity.class);
                    intent.putExtra("url", articles.get(getAdapterPosition()).getLink());
                    context.startActivity(intent);
                    return true;
                }
            });

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != tts) {
                        stopTTS();
                    }


                    tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
                        @Override
                        public void onInit(int status) {
                            String lang = AppStorage.getLang();
                            if (status == TextToSpeech.SUCCESS) {
                                tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                                    @Override
                                    public void onDone(String utteranceId) {
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                isPLaying = false;
                                                title.setTextColor(Color.parseColor(COLOR2));
                                                date.setTextColor(Color.parseColor(COLOR3));
                                                description.setTextColor(Color.parseColor(COLOR3));
                                                id = -1;
                                                notifyDataSetChanged();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onError(String utteranceId) {
                                    }

                                    @Override
                                    public void onStart(String utteranceId) {
                                    }

                                });

                                int result = 9999;
                                if (lang.equals("es")) {
                                    result = tts.setLanguage(new Locale("spa", "MEX"));
                                } else if (lang.equals("en")) {
                                    result = tts.setLanguage(new Locale("eng", "GB"));
                                } else if (lang.equals("fr")) {
                                    result = tts.setLanguage(new Locale("fra", "FR"));
                                } else if (lang.equals("de")) {
                                    result = tts.setLanguage(new Locale("de", "DE"));
                                }

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                                    Log.e("TTS", "This Language is not supported");
                                } else {
                                    if (!isPLaying && id != getAdapterPosition()) {
                                        playback();
                                        id = getAdapterPosition();
                                        title.setTextColor(Color.parseColor(COLOR1));
                                        date.setTextColor(Color.parseColor(COLOR1));
                                        description.setTextColor(Color.parseColor(COLOR1));
                                    } else {
                                        stopTTS();
                                        title.setTextColor(Color.parseColor(COLOR2));
                                        date.setTextColor(Color.parseColor(COLOR3));
                                        description.setTextColor(Color.parseColor(COLOR3));
                                        id = -1;
                                    }
                                    notifyDataSetChanged();

                                }

                            } else {
                                Log.e("TTS", "Initilization Failed!");
                            }
                        }

                        private void playback() {
                            isPLaying = true;
                            String t = title.getText().toString();
                            String d = description.getText().toString();
                            Bundle params = new Bundle();
                            params.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "UniqueID");
                            tts.speak(t + "." + d, TextToSpeech.QUEUE_ADD, params, "UniqueID");

                        }
                    });

                }
            });
        }
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PaperViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.feed, viewGroup, false);
        return new PaperViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PaperViewHolder personViewHolder, final int i) {
        Article article = articles.get(i);
        String title = Utils.stripHtml(article.getTitle());
        String description = Utils.stripHtml(null != article.getDescription() ? article.getDescription() : "");
        personViewHolder.setIsRecyclable(false);
        personViewHolder.title.setText(title);
        personViewHolder.description.setText(description);

        if (null != article.getPubDate()) {
            String date = Utils.getStringTimeStampWithDate(lang, article.getPubDate());
            personViewHolder.date.setText(date);
        } else {
            personViewHolder.date.setText("Description : ");
        }

        if (i == id) {
            personViewHolder.title.setTextColor(Color.parseColor(COLOR1));
            personViewHolder.date.setTextColor(Color.parseColor(COLOR1));
            personViewHolder.description.setTextColor(Color.parseColor(COLOR1));

        } else {
            personViewHolder.title.setTextColor(Color.parseColor(COLOR2));
            personViewHolder.date.setTextColor(Color.parseColor(COLOR3));
            personViewHolder.description.setTextColor(Color.parseColor(COLOR3));

        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    private void getArticles() {
        parser = new Parser();
        parser.execute(paper.getUrl());
        parser.onFinish(new Parser.OnTaskCompleted() {

            @Override
            public void onTaskCompleted(ArrayList<Article> list) {
                articles = list;
                activity.onFinishRender();
            }

            @Override
            public void onError() {
                Toast.makeText(context, "Unable to retrieve RSS feeds from " + paper.getTitle(), Toast.LENGTH_LONG).show();
                activity.onFinishRender();
            }
        });
    }


}
