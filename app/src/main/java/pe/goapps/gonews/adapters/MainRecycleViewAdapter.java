package pe.goapps.gonews.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pe.goapps.gonews.R;
import pe.goapps.gonews.model.Paper;
import pe.goapps.gonews.store.AppStorage;
import pe.goapps.gonews.utils.JsonUtils;
import pe.goapps.gonews.views.FeedsActivity;

public class MainRecycleViewAdapter extends RecyclerView.Adapter<MainRecycleViewAdapter.PaperViewHolder> {
    private List<Paper> papers;
    private Context context;


    public MainRecycleViewAdapter(Context context) {
        this.context = context;
        papers = parseJSONtoPapers(JsonUtils.getPaperListString());

    }

    public class PaperViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView title;
        TextView country;
        ImageView img;
        private final String LOG_TAG = "RVAdapter";

        PaperViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cv);
            title = (TextView) itemView.findViewById(R.id.title);
            country = (TextView) itemView.findViewById(R.id.country);
            img = (ImageView) itemView.findViewById(R.id.img);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Paper paper = papers.get(getAdapterPosition());
                    Intent feedsIntent = new Intent(context, FeedsActivity.class);
                    feedsIntent.putExtra("paperJson", JsonUtils.paperToJSonString(paper));
                    feedsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    AppStorage.setLang(paper.getLang());
                    context.startActivity(feedsIntent);
                }
            });
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PaperViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item, viewGroup, false);
        PaperViewHolder pvh = new PaperViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PaperViewHolder personViewHolder, final int i) {
        Paper paper = papers.get(i);
        personViewHolder.title.setText(paper.getTitle());
        personViewHolder.country.setText(paper.getCountry());
        personViewHolder.img.setImageResource(context.getResources().getIdentifier("drawable/" + paper.getImg(), null, context.getPackageName()));
    }

    @Override
    public int getItemCount() {
        return papers.size();
    }

    private List<Paper> parseJSONtoPapers(String jsonStr) {
        List<Paper> news = new ArrayList<>();
        JSONArray objects = null;
        try {
            objects = new JSONArray(jsonStr);
            for (int i = 0; i < objects.length(); i++) {
                JSONObject json = objects.getJSONObject(i);
                Paper paper = new Paper();
                paper.setId(json.getInt("id"));
                paper.setTitle(json.getString("title"));
                paper.setLang(json.getString("lang"));
                paper.setCountry(json.getString("country"));
                paper.setImg(json.getString("img"));
                paper.setUrl(json.getString("url"));
                news.add(paper);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return news;

    }
}
