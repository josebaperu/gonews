package pe.goapps.gonews.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import pe.goapps.gonews.R;
import pe.goapps.gonews.adapters.FeedsAdapter;
import pe.goapps.gonews.model.Paper;
import pe.goapps.gonews.utils.JsonUtils;

public class FeedsActivity extends BaseActivity {
    private ImageView paperImg;
    private TextView paperHeader;
    private Paper paper;
    private Context context;
    private FeedsAdapter adapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager llm;
    private ProgressBar progressBar;
    private final String LOG_TAG = "FeedsActivity";

    public void cancelTTS(View v) {
        adapter.stopTTS();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        Intent intent = getIntent();
        String paperStr = intent.getStringExtra("paperJson");
        paper = JsonUtils.jsonToPaper(paperStr);
        setContentView(R.layout.activity_feeds);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable().setColorFilter(0xFF777777, android.graphics.PorterDuff.Mode.MULTIPLY);
        progressBar.setVisibility(View.VISIBLE);
        paperImg = findViewById(R.id.paperImg);
        paperImg.setImageResource(context.getResources().getIdentifier("drawable/" + paper.getImg(), null, context.getPackageName()));
        paperHeader = findViewById(R.id.paperHeader);
        paperHeader.bringToFront();
        paperHeader.setText(paper.getTitle());
        recyclerView = (RecyclerView) findViewById(R.id.rvFeeds);
        llm = new LinearLayoutManager(context);
        recyclerView.setKeepScreenOn(true);
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);
        adapter = new FeedsAdapter(this, paper);
        recyclerView.setAdapter(adapter);
    }

    public void onFinishRender() {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopTTS();
    }
}
