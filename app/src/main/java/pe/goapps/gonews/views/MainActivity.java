package pe.goapps.gonews.views;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import pe.goapps.gonews.R;
import pe.goapps.gonews.adapters.MainRecycleViewAdapter;

public class MainActivity extends BaseActivity {
    private RecyclerView rv;
    private LinearLayoutManager llm;
    private static final String LOG_TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv = (RecyclerView) findViewById(R.id.rv);
        llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        rv.setAdapter(new MainRecycleViewAdapter(this));
    }
}