package pe.goapps.gonews.app;

import android.app.Application;
import android.content.Context;

import pe.goapps.gonews.utils.AdBlocker;

public class GoNews extends Application {
    public final static String LOG_TAG = "APPLICATION";
    private static Application sApplication;

    public static Application getApplication() {
        return sApplication;
    }

    public static Context getContext() {
        return getApplication().getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
        AdBlocker.init(this);
    }
}
