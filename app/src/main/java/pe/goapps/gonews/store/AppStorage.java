package pe.goapps.gonews.store;

import android.content.Context;
import android.content.SharedPreferences;

import pe.goapps.gonews.app.GoNews;

import static android.content.Context.MODE_PRIVATE;

public class AppStorage {

    static private Context context = GoNews.getContext();

    private static SharedPreferences settings = context.getSharedPreferences("GONEWS", MODE_PRIVATE);
    private static SharedPreferences.Editor editor;


    public static String getLang() {
        return settings.getString("LANG", "es");
    }

    public static void setLang(String lang) {
        editor = settings.edit();
        editor.putString("LANG", lang);
        editor.apply();
    }
}
